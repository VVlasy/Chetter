﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace VVlasy.Chetter.Windows
{
    public static class StaticExtensions
    {
        public static void Notify(this PropertyChangedEventHandler handler, object sender, string property)
        {
            handler?.Invoke(sender, new PropertyChangedEventArgs(property));
        }

        public static bool HasFlag(this ModifierKeys sender, params ModifierKeys[] modifiers)
        {
            foreach (var item in modifiers)
                if (sender.HasFlag(item))
                    return true;

            return false;
        }
    }
}
