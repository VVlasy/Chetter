﻿using System.Windows;

namespace VVlasy.Chetter.Windows.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var model = DataContext as MainWindowModel;

            e.Handled = model?.TextBox_PreviewKeyDown(e.Key, e.KeyboardDevice) ?? false;
        }
    }
}
