﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VVlasy.Chetter.Library;

namespace VVlasy.Chetter.Windows.UI
{
    public class MainWindowModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand SendCommand { get; set; }

        public ICommand ClearCommand { get; set; }

        private MessengerProvider messenger;

        private string _displayedText;

        public string DisplayedText
        {
            get
            {
                return _displayedText;
            }

            set
            {
                _displayedText = value;
                PropertyChanged.Notify(this, "DisplayedText");
            }
        }

        private string _messages = string.Empty;

        public string Messages
        {
            get
            {
                return _messages;
            }

            set
            {
                _messages += value + "\r\n";
                PropertyChanged.Notify(this, "Messages");
            }
        }

        private string _messageToSend;

        public string MessageToSend
        {
            get
            {
                return _messageToSend;
            }

            set
            {
                _messageToSend = value;
                PropertyChanged.Notify(this, "MessageToSend");
            }
        }

        public MainWindowModel()
        {
            SendCommand = new RelayCommand(p => !string.IsNullOrEmpty(MessageToSend), p => Send());
            ClearCommand = new RelayCommand(p => !string.IsNullOrEmpty(_messages), p => Clear());

            messenger = new MessengerProvider(6656);
            messenger.ReceivedMessage += Messenger_ReceivedMessage;
        }

        public bool TextBox_PreviewKeyDown(Key pressedKey, KeyboardDevice kybDevice)
        {
            if (pressedKey == Key.Enter && !kybDevice.Modifiers.HasFlag(ModifierKeys.Shift, ModifierKeys.Control, ModifierKeys.Alt))
                Send();
            else
                return false;

            return true;
        }

        private void Messenger_ReceivedMessage(object sender, string e)
        {
            Messages = $"{DateTime.Now.ToString()}: {e}";
        }

        private void Send()
        {
            if (string.IsNullOrEmpty(MessageToSend))
                return;

            messenger.SendMessage(MessageToSend);
            Messages = "You: " + MessageToSend;
            MessageToSend = string.Empty;
        }

        private void Clear()
        {
            _messages = string.Empty;
            PropertyChanged.Notify(this, "Messages");
        }
    }
}
