﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VVlasy.Chetter.Library
{
    public class MessengerProvider
    {
        private UdpClient listener;

        private CancellationTokenSource cancellationTokenSource;

        private CancellationToken cancellationToken;

        private int _listenerPort;

        public event EventHandler<string> ReceivedMessage;

        public int ListenerPort
        {
            get
            {
                return _listenerPort;
            }

            set
            {
                _listenerPort = value;

                if (listener != null)
                {
                    cancellationTokenSource.Cancel();
                    listener.Client.Shutdown(SocketShutdown.Both);
                }

                listener = new UdpClient();
                listener.Client.Bind(new IPEndPoint(IPAddress.Any, ListenerPort));
                listener.EnableBroadcast = true;

                Task.Run(async () =>
                {
                    while (true)
                    {
                        if (cancellationToken.IsCancellationRequested)
                            cancellationToken.ThrowIfCancellationRequested();

                        var receivedResults = await listener.ReceiveAsync();
                        if (!await IsIpLocal(receivedResults.RemoteEndPoint))
                            ReceivedMessage?.Invoke(this, Encoding.UTF8.GetString(receivedResults.Buffer));
                    }
                });
            }
        }

        private UdpClient sender;

        private int _senderPort;

        public int SenderPort
        {
            get
            {
                return _senderPort;
            }

            set
            {
                _senderPort = value;

                sender = new UdpClient();
                sender.EnableBroadcast = true;
            }
        }

        public MessengerProvider(int listenPort, int sendPort = 0)
        {
            cancellationTokenSource = new CancellationTokenSource();
            cancellationToken = cancellationTokenSource.Token;

            ListenerPort = listenPort;
            SenderPort = sendPort == 0 ? listenPort : sendPort;
        }

        public MessengerProvider(int port)
        {
			cancellationTokenSource = new CancellationTokenSource();
			cancellationToken = cancellationTokenSource.Token;

			ListenerPort = port;
            SenderPort = ListenerPort;
        }

        public void SendMessage(string msg)
        {
            SendMessage(msg, SenderPort);
        }

        public void SendMessage(string msg, int port)
        {
            var message = Encoding.UTF8.GetBytes(msg);
            sender.SendAsync(message, message.Length, new IPEndPoint(IPAddress.Broadcast, port)).Wait();
        }

        private async Task<bool> IsIpLocal(IPEndPoint ip)
        {
            var islocal = false;
            foreach (IPAddress item in await Dns.GetHostAddressesAsync(Dns.GetHostName()))
            {
                if (ip.Address.ToString() == item.ToString())
                    islocal = true;
            }

            return islocal;
        }
    }
}