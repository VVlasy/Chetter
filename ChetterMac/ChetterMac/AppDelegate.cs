﻿using AppKit;
using Foundation;

namespace EasyJobTest
{
    [Register("AppDelegate")]
    public class AppDelegate : NSApplicationDelegate
    {
        public AppDelegate()
        {
        }

        public override void DidFinishLaunching(NSNotification notification)
        {
            // Insert code here to initialize your application
        }

        public override void WillTerminate(NSNotification notification)
        {
            // Insert code here to tear down your application
        }

		public override bool ApplicationShouldHandleReopen(NSApplication sender, bool hasVisibleWindows)
		{
            if (!hasVisibleWindows)
            {
                for (int i = 0; i < NSApplication.SharedApplication.Windows.Length; i++)
                {
                    var content = NSApplication.SharedApplication.Windows[i].ContentViewController as ViewController;
                    if (content != null)
                    {
                        NSApplication.SharedApplication.Windows[i].MakeKeyAndOrderFront(this);
                        return true;
                    }
                }
            }

            return true;
		}
    }
}
