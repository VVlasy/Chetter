﻿﻿﻿using System;

using AppKit;
using Foundation;
using VVlasy.Chetter.Library;
namespace EasyJobTest
{
    public partial class ViewController : NSViewController
    {
        private MessengerProvider messenger;

        private string _messages = string.Empty;

        public string Messages
        {
            get
            {
                return _messages;
            }
            set
            {
                _messages += value + "\r\n";
                MessagesBox.StringValue = Messages;
            }
        }
        
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            messenger = new MessengerProvider(6656);
            messenger.ReceivedMessage += OnMessageReceived;
        }

        private void OnMessageReceived(object sender, string e)
        {
            InvokeOnMainThread(() =>
            {
                Messages = e;
            });

            ShowUserNotification(e);
        }

        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
                // Update the view, if already loaded.
            }
        }

		partial void ActionButton(Foundation.NSObject sender)
		{
            var message = MessageToSendBox.StringValue;

            if (string.IsNullOrEmpty(message))
                return;
            
            messenger.SendMessage(message);
            Messages = "You: " + message;
            MessageToSendBox.StringValue = string.Empty;
		}

		private void ShowUserNotification(string text)
		{
			// Trigger a local notification after the time has elapsed
			var notification = new NSUserNotification();

			// Add text and sound to the notification
			notification.Title = "New message!";
			notification.InformativeText = text;
			notification.SoundName = NSUserNotification.NSUserNotificationDefaultSoundName;
			notification.HasActionButton = true;
			NSUserNotificationCenter.DefaultUserNotificationCenter.DeliverNotification(notification);
		}
    }
}
