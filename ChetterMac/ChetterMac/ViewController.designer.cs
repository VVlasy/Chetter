// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace EasyJobTest
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        AppKit.NSTextField DisplayedText { get; set; }

        [Outlet]
        AppKit.NSTextField MessagesBox { get; set; }

        [Outlet]
        AppKit.NSTextField MessageToSendBox { get; set; }

        [Action ("ActionButton:")]
        partial void ActionButton (Foundation.NSObject sender);
        
        void ReleaseDesignerOutlets ()
        {
            if (MessagesBox != null) {
                MessagesBox.Dispose ();
                MessagesBox = null;
            }

            if (MessageToSendBox != null) {
                MessageToSendBox.Dispose ();
                MessageToSendBox = null;
            }

            if (DisplayedText != null) {
                DisplayedText.Dispose ();
                DisplayedText = null;
            }
        }
    }
}
